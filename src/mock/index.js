import Mock from 'mockjs'

import tableAPI from './user'

// 用户相关
Mock.mock(/\/user\/login/, 'post', tableAPI.login)
Mock.mock(/\/user\/login/, 'get', tableAPI.login)
Mock.mock(/\/user\/getUser/, 'get', tableAPI.getUserList)
Mock.mock(/\/user\/remove/, 'get', tableAPI.deleteUser)
Mock.mock(/\/user\/batchremove/, 'get', tableAPI.batchremove)
Mock.mock(/\/user\/add/, 'get', tableAPI.createUser)
Mock.mock(/\/user\/edit/, 'get', tableAPI.updateUser)
