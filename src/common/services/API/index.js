/** 
 * api接口的统一出口
*/
// import Vue from "vue";
// 登录模块接口
import login from '@/common/services/API/module/login';
// 曲线模块
import chart from '@/common/services/API/module/chart';
// 表格模块
import table from '@/common/services/API/module/table';
// 人员模块
import user from '@/common/services/API/module/user';
// 项目设置模块
import project from '@/common/services/API/module/project';

const api = {
    login,
    chart,
    table,
    user,
    project
}


// Vue.prototype.$api = api; // 将api挂载到vue的原型上

// // 导出接口
export default api