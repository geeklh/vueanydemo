/**
 * login模块接口列表
 */

import base from '../base'; // 导入接口域名列表
import axios from '@/common/services/http'; // 导入http中创建的axios实例
import qs from 'qs'; // 导入qs模块
// import { log } from 'util';

const login = {
    // // 新闻列表    
    articleList() {
        let url = "http://www.phonegap100.com/appapi.php?a=getPortalList&catid=20&page=1"
        return axios.get(url);
    },
    // // 新闻详情,演示    
    // articleDetail(id, params) {
    //     return axios.get(`${base.sq}/topic/${id}`, {
    //         params: params
    //     });
    // },
    // post提交    
    getToken(params) {
        console.log(11);
        return axios.post(`${base.td}/api/user/login`, params);
    }
    // 其他接口…………
}

export default login;