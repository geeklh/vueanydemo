import zhLocale from 'element-ui/lib/locale/lang/zh-CN'
const zh = {
    public: {
        title: '监测云平台',
        tips: '提示',
        changeLanguage: '确定切换语言吗？'
    },
    login: {
        account: '账号',
        pass: '密码',
        rememberPassword: '记住密码',
        loginButton: '登录',
        rules: {
            account: {
                message1: '请输入账户名',
                message2: ''
            },
            pass: {
                message1: '请输入密码',
                message2: '长度最短 3 个字符'
            }
        },
        message: {
            success: '登录成功!',
            failure: '登录失败!',
        }
    },
    operate: {
        add: '添加',
        delete: '删除',
        edit: '编辑',
        clean: '清空',
        setup: '设置',
        import: '导入',
        upload: '上传',
        entry: '输入',
        determine: '确定',
        cancel: '取消',
        send: '发送',
    },
    message: {
        tips: '提示',
        changeLanguage: '确定切换语言吗？',
        success: "成功！",
        failure: '失败!',
    },
    ...zhLocale
}
export default zh;