import enlocale from "element-ui/lib/locale/lang/en";
const en = {
    public: {
        title: 'Monitoring cloud platform',
    },
    login: {
        account: 'Account',
        pass: 'Password',
        rememberPassword: 'remember password',
        loginButton: 'Login',
        rules: {
            account: {
                message1: 'please input username',
                message2: ''
            },
            pass: {
                message1: 'please input password',
                message2: 'Minimum 3 characters'
            }
        },
        message: {
            success: 'login successful!',
            failure: 'login failure!',
        }
    },
    operate: {
        add: 'Add',
        delete: 'Delete',
        edit: 'Edit',
        clean: 'Clean',
        setup: 'Setup',
        import: 'Import',
        upload: 'Upload',
        entry: 'Entry',
        determine: 'Determine',
        cancel: 'Cancel',
        send: 'Send',
    },
    message: {
        tips: 'tips',
        changeLanguage: 'Are you sure to switch languages?',
        success: 'OK!',
        failure: 'failure!',
    },
    ...enlocale
}
export default en;