// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// import Vuex from 'vuex'
import store from "./store"
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'
import './style/index.scss'
// 样式重置
import 'normalize.css'
//国际化插件
import i18n from "./common/i18n/i18n"
// mock
import "./mock"
// 导入接口
import api from "@/common/services/API/index"
// Echart
import echarts from 'echarts'
import "@/utils/chart/index.js"
Vue.use(echarts)
// Vue.prototype.$echarts = echarts

Vue.config.productionTip = false

Vue.prototype.$api = api; // 将api挂载到vue的原型上

Vue.use(ElementUI);
// Vue.use(Vuex);


/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  i18n,
  components: {
    App
  },
  template: '<App/>'
})
