//表格配置化参数
let Attributes = {
    filterConditions: {
        filters: [
            {
                type: 'date-picker',
                align: 'left',
                startPlaceholder: '开始日期',
                rangeSeparator: '至',
                endPlaceholder: '结束日期',
                unlinkPanels: true,
                pickerOptions: {
                    shortcuts: [{
                        text: '最近一周',
                        onClick(picker) {
                            const end = new Date();
                            const start = new Date();
                            start.setTime(start.getTime() - 3600 * 1000 * 24 * 7);
                            picker.$emit('pick', [start, end]);
                        }
                    }, {
                        text: '最近一个月',
                        onClick(picker) {
                            const end = new Date();
                            const start = new Date();
                            start.setTime(start.getTime() - 3600 * 1000 * 24 * 30);
                            picker.$emit('pick', [start, end]);
                        }
                    }, {
                        text: '最近三个月',
                        onClick(picker) {
                            const end = new Date();
                            const start = new Date();
                            start.setTime(start.getTime() - 3600 * 1000 * 24 * 90);
                            picker.$emit('pick', [start, end]);
                        }
                    }]
                },
            },

        ]
    },
    TableAttributes: {
        border: true,//边框
        size: 'mini',//表格尺寸
        // height: '',//高度
        maxHeight: '600',//最大高度
        stripe: true,//条纹
        fit: true,//列撑开
        showHeader: true,//表头
        lazy: true,

    },
    TableColumnAttributes: {
        isSelection: true, //多选
        isIndex: true, //行序号
        indexLabel: "序号", //行序号标题
        tableCols: {
            cols: [
                {
                    id: 0,
                    show: false,
                    prop: 'date',
                    label: '日期',
                    // width: '180',
                    align: 'left',
                    sortable: true

                },
                {
                    id: 1,
                    show: true,
                    prop: 'name',
                    label: '姓名',
                    // width: '180',
                    align: 'left',
                    sortable: true
                },
                {
                    id: 2,
                    show: true,
                    prop: 'address',
                    label: '地址',
                    // width: '180',
                    align: 'left',
                    sortable: true
                },
                {
                    id: 3,
                    show: true,
                    prop: 'data1',
                    label: '地址1',
                    // width: '180',
                    align: 'left',
                    sortable: true
                },
                {
                    id: 4,
                    show: true,
                    prop: 'data2',
                    label: '地址2',
                    // width: '180',
                    align: 'left',
                    sortable: true
                },
                {
                    id: 5,
                    show: true,
                    prop: 'data3',
                    label: '地址3',
                    // width: '180',
                    align: 'left',
                    sortable: true
                },
                {
                    id: 6,
                    show: true,
                    prop: 'data4',
                    label: '地址4',
                    // width: '180',
                    align: 'left',
                    sortable: true
                },
                {
                    id: 7,
                    show: true,
                    prop: 'data5',
                    label: '地址5',
                    // width: '180',
                    align: 'left',
                    sortable: true
                },
                {
                    id: 8,
                    show: true,
                    prop: 'data6',
                    label: '地址6',
                    // width: '180',
                    align: 'left',
                    sortable: true
                },
                {
                    id: 9,
                    show: true,
                    prop: 'data7',
                    label: '地址7 ',
                    // width: '180',
                    align: 'left',
                    sortable: true
                },

            ],
            colButtons: {
                isButton: true,//操作按钮
                buttonLabel: '操作',//操作列标题
                width: '150',
                align: 'center',
                buttons: [
                    {
                        type: 'edit',//类型
                        text: '编辑',//按钮文字
                        size: 'mini',//按钮大小
                        btnType: 'primary',//按钮样式
                        handle: (index, row) => {//按钮操作函数
                            console.log("编辑", index, row);
                        }
                    },
                    {
                        type: 'delete',
                        text: '删除',
                        size: 'mini',
                        btnType: 'danger',
                        handle: (index, row) => {
                            console.log("删除", index, row);
                        }
                    }
                ]
            }

        }
    }
}

export { Attributes };