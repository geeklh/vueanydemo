import Vue from "vue";


/**
 * 曲线自适应重绘
 * @param("曲线对象") ref
 */
Vue.prototype.$echartsResize = function (ref) {
    window.addEventListener("resize", function () {
        console.log("window.resize执行");
        // Vue.$throttle2(ref.resize(), 300);
        // ref.resize();
    })
}

/**
 * 节流函数
 * @param("执行函数") fn
 * @param("间隔时间") delay 
 */
Vue.prototype.$throttle2 = function (fn, delay) {
    return args => {
        if (fn.id) return
        fn.id = setTimeout(() => {
            console.log("节流执行");
            fn.call(this, args);
            clearTimeout(fn.id);
            fn.id = null;
        }, delay);
    }
}
