import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)


// import Login from '@/Views/Login'
// import Home from '@/Views/Home'
const Login = () => import('@/Views/Login/Login.vue')
// 首页
const Home = () => import('@/Views/Home/Home.vue')
const ProjactList = () => import('@/Views/ProjectList/ProjectList.vue')
const UnitInformation = () => import('@/Views/ProjectManage/UnitInformation.vue')
const Monitor = () => import('@/Views/ProjectManage/Monitor.vue')
const Supervision = () => import('@/Views/ProjectManage/Supervision.vue')
const ProjectList = () => import('@/Views/ProjectManage/ProjectList.vue')
const Err = () => import('@/Views/404.vue')
const Text = () => import('@/Views/text.vue')
// 项目详情
const Index = () => import('@/Views/Index/Index.vue')
const EchartList = () => import('@/Views/Chart/EchartList.vue')
const Echart = () => import('@/Views/Chart/Echart.vue')
const DataTable = () => import('@/Views/Table/DataTable.vue')

// 解决点击相同路由报错误问题
const originalPush = Router.prototype.push;
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err);
};


const router = new Router({
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        requireAuth: false
      }
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      meta: {
        requireAuth: true
      },
      children: [
        {
          path: 'projactList',
          component: ProjactList,
          name: '项目列表',
          meta: { requireAuth: true }
        },
        {
          path: 'unitInformation',
          component: UnitInformation,
          name: '单位信息',
          meta: { requireAuth: true }
        },
        {
          path: 'monitor',
          component: Monitor,
          name: '监测人员管理',
          meta: { requireAuth: true }
        },
        {
          path: 'supervision',
          component: Supervision,
          name: '监督人员管理',
          meta: { requireAuth: true }
        },
        {
          path: 'projectList',
          component: ProjectList,
          name: '项目管理',
          meta: { requireAuth: true }
        }
      ]
    },
    {
      path: '/Err',
      name: 'err',
      component: Err,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/Text',
      name: 'text',
      component: Text,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/Index',
      name: '项目详情',
      component: Index,
      meta: {
        requireAuth: true
      },
      children: [
        {
          path: 'EchartList',
          component: EchartList,
          name: '数据曲线',
          meta: { requireAuth: true }
        },
        {
          path: 'Echart',
          component: Echart,
          name: '曲线',
          meta: { requireAuth: true }
        },
        {
          path: 'DataTable',
          component: DataTable,
          name: '表格封装测试',
          meta: { requireAuth: true }
        },



      ]
    },
    // {
    //   path: '/Echart',
    //   name: 'echart',
    //   component: Echart,
    //   meta: {
    //     requireAuth: true
    //   }
    // },
    {
      path: '*',
      component: Login,
      meta: {
        requireAuth: false
      }
    }
  ]
})



router.beforeEach((to, from, next) => {
  console.log("to:", to);
  console.log("form:", from);
  console.log("next:", next);

  if (to.matched.some(record => record.meta.requireAuth)) { // 判断该路由是否需要登录权限
    const token = sessionStorage.getItem('token');
    if (token) { // 判断缓存里面是否有 token  //在登录的时候设置它的值
      next();
    } else {
      next({
        path: '/login',
        query: {
          redirect: to.fullPath
        } // 将跳转的路由path作为参数，登录成功后跳转到该路由
      })
    }
  } else {
    next();
  }
});

/**
 * 默认无权限路由页面
 */
export const constantRouterMap = [
  {
    path: '/login',
    name: 'login',
    component: Login,
    meta: {
      requireAuth: false
    }
  },
  {
    path: '/home',
    name: 'home',
    component: Home,
    meta: {
      requireAuth: true
    },
    children: [
      {
        path: 'projactList',
        component: ProjactList,
        name: '项目列表',
        meta: { requireAuth: true }
      },
      {
        path: 'unitInformation',
        component: UnitInformation,
        name: '单位信息',
        meta: { requireAuth: true }
      },
      {
        path: 'monitor',
        component: Monitor,
        name: '监测人员管理',
        meta: { requireAuth: true }
      },
      {
        path: 'supervision',
        component: Supervision,
        name: '监督人员管理',
        meta: { requireAuth: true }
      },
      {
        path: 'projectList',
        component: ProjectList,
        name: '项目管理',
        meta: { requireAuth: true }
      }
    ]
  },
  {
    path: '/Err',
    name: 'err',
    component: Err,
    meta: {
      requireAuth: true
    }
  },
  {
    path: '/Text',
    name: 'text',
    component: Text,
    meta: {
      requireAuth: true
    }
  }
]

/**
 * 带权限路由页面
 */
export const asyncRouterMap = [{}]

/**
 * 路由注册
 */
export default new Router({
  mode: 'history', // 默认为'hash'模式
  base: '/permission/', // 添加根目录,对应服务器部署子目录
  routes: constantRouterMap
})

// export default router




