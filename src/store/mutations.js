/**
 * 包含多个由action触发，去直接更新状态的方法的对象
 */
import { IS_MOBILE, ADD_TOKEN, DEL_TOKEN, CHANGE_PROJECTID } from "./mutation-types";
export default {
    [IS_MOBILE](state, { mobileType }) {
        console.log("++mobileType", mobileType);
        state.isMobile = mobileType;
        console.log("vuex更改后", state.isMobile);

    },
    [ADD_TOKEN](state, { token }) {
        state.token = token;
    },
    [DEL_TOKEN](state) {
        state.token = '';
    },
    [CHANGE_PROJECTID](state, { projectId }) {
        state.projectId = projectId;
    }
};
