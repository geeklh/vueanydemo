/**
 * mutation 的名称常量
 */
export const IS_MOBILE = 'is_mobile';//是否手机
export const ADD_TOKEN = 'add_token';//添加token
export const DEL_TOKEN = 'del_token';//删除token
export const CHANGE_PROJECTID = 'change_projectid';//修改项目编号

