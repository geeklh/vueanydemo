/**
 * 包含多个接收组件通知触发mutations调用间接状态的方法的对象
 */
import { IS_MOBILE, ADD_TOKEN, DEL_TOKEN, CHANGE_PROJECTID } from "./mutation-types";
export default {
    // 判断设备类型
    isMobile({ commit }, mobileType) {
        console.log("mobileType", mobileType);
        commit(IS_MOBILE, { mobileType })
    },
    // 添加token
    addToken({ commit }, token) {
        // 提交对comutations 请求
        commit(ADD_TOKEN, { token });
    },
    // token
    delToken({ commit }) {
        // 提交对comutations 请求
        commit(DEL_TOKEN);
    },
    //修改项目编号
    changeProjectId({ commit }, projectId) {
        commit(CHANGE_PROJECTID, { projectId });
    },
}