/**
 * 包含所有的的基于state和getter计算属性的对象
 */
export default {
    // 获取设备类型
    getMObileType(state) {
        return state.isMobile
    },
    //获取token
    getToken(state) {
        return state.token
    },
    //获取项目编号
    getProjectId(state) {
        return state.projectId
    }
}